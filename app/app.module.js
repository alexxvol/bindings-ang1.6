'use strict';

// Define the `phonecatApp` module
angular.module('mainApp', [])
    .component('outerComp', {
        bindings: {
            usernameb: '@',
            //@ string binding,             //< object binding               //& function binding
        },
        controller: [function() {
            var $ctrl = this;
            $ctrl.names = ['nick'];
            //все что хотим отобразить до загрузки в $onInit 
            $ctrl.$onInit = function() {
                $ctrl.username = $ctrl.usernameb || 'NA';
            };

            $ctrl.addName = function(name) {
                $ctrl.username = name;
                $ctrl.names.push(name);
            };
        }],
        template: `
        <h2>Hello from the outer component</h2>
        <p>User name is: {{$ctrl.username}}</p>
        <inner-comp add-name = "$ctrl.addName(name)"></inner-comp>
        <hr />
        <p ng-repeat = 'name in $ctrl.names'>
          {{name}}
        </p>
        `
    })
    .component('innerComp', {
        bindings: {
            addName: '&'
        },
        controller: [function() {
            var $ctrl = this;
            $ctrl.addAName = function() {

                $ctrl.addName({ name: $ctrl.newName });
                $ctrl.newName = '';
            }
        }],
        template: ` <h3> Hello from the inner component </h3>
                    <p> new Name: <input ng-model =  "$ctrl.newName"> </p>
                    <p><button ng-click = "$ctrl.addAName()"> Add name:</button><p>
        `
    });